const { SlashCommandBuilder } = require('@discordjs/builders');
const { MessageEmbed } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('contact')
		.setDescription('Contact information for chatting and feedback.'),
	async execute(interaction) {
		const contact = new MessageEmbed()
			.setTitle('Hayase - Made by voltages/v4ltages')
			.addFields(
				{ name: 'Email:', value: 'contact@voltages.me' },
				{ name: 'Community Gitter', value: 'https://gitter.im/hayase-discordbot/dev' },
				{ name: 'For issue reporting:', value: 'Use /issue command or make a issue on GitLab: https://gitlab.com/v4ltages/hayase/-/issues' },
			)
			.setFooter({ text: 'Thank you ❤️' });
		return interaction.reply({ embeds:[contact], ephemeral: true });
	},
};
