const { SlashCommandBuilder } = require('@discordjs/builders');
const { duelarray } = require('../../utils.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('duel')
		.setDescription('A grand duel to see who wins.')
		.addUserOption(option => option.setName('mention').setDescription('Select a user to duel').setRequired(true)),
	async execute(interaction) {
		const user = interaction.options.getUser('mention');
		const responceList = [
			`${interaction.member} got pinned down by ${user} and lost!`,
			`${interaction.member} managed to pin down ${user} and won!`,
			`${interaction.member} does a sweeping hip move on ${user} but fails and trips, losing!`,
			`${interaction.member} does a sweeping hip move on ${user} and tackles them, winning!`,
			`${interaction.member} grabbed ${user} and causes them to trip. winning!`,
			`${interaction.member} grabbed ${user} but you slip, losing!`,
		];
		return interaction.reply(`${interaction.member} duels ${user}!\n\n${responceList[Math.floor(Math.random() * responceList.length)]}\n${duelarray[Math.floor(Math.random() * duelarray.length)]}`);
	},
};