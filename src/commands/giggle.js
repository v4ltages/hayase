const { SlashCommandBuilder } = require('@discordjs/builders');
const { gigglearray } = require('../../utils');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('giggle')
		.setDescription('*Giggles at senpai*')
		.addUserOption(option => option.setName('mention').setDescription('Select a user to giggle at.')),
	async execute(interaction) {
		const user = interaction.options.getUser('mention');
		if (user) return interaction.reply(`${interaction.member} giggles at ${user}\n ${gigglearray[Math.floor(Math.random() * gigglearray.length)]}`);
		interaction.reply(`${interaction.member} giggles. \n${gigglearray[Math.floor(Math.random() * gigglearray.length)]}`);
	},
};