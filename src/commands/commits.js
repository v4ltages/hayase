const axios = require('axios');
const { MessageEmbed } = require('discord.js');
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('commits')
		.setDescription('Lists all recent 5 commits done to Hayase via GitLabs API'),
	async execute(interaction) {
		axios.get('https://gitlab.com/api/v4/projects/18199726/repository/commits?ref_name=master').then(response => {
			const commitlist = new MessageEmbed()
				.setColor('#df4329')
				.setTitle('List of recent commits to Hayase')
				.addFields(...response.data.slice(0, 5).map(commit => ({ name: `#${commit.short_id} - ${commit.author_name}`, value: `${commit.title} \n [[Link]](${commit.web_url})` })))
				.setTimestamp();
			interaction.reply({ embeds: [commitlist], ephemeral: true });
		})
			.catch((error) => {
				if (error.response) {
					interaction.reply({ text: 'Sorry, the request was made and the server responded with a status code that falls out of the range of 2xx', ephemeral: true });
				}
				else if (error.request) {
					console.log(error.request);
					interaction.reply({ text: 'Sorry, no response was received...', ephemeral: true });
				}
			});
	},
};