const { byearray } = require('../../utils');
const { SlashCommandBuilder } = require('@discordjs/builders');

const responceList = [
	'is heading out. 👋',
	'is leaving. 👋',
	'says bye bye 👋',
	'says take care 👋',
];

module.exports = {
	data: new SlashCommandBuilder()
		.setName('bye')
		.setDescription('Bye bye!')
		.addStringOption(option => option.setName('input').setDescription('What you want it to say or mention?')),
	async execute(interaction) {
		const input = interaction.options.getString('input');
		if (input) return interaction.reply(`${interaction.member} *says see you* ${input}\n${byearray}`);
		return interaction.reply(`${interaction.member} *${responceList[Math.floor(Math.random() * responceList.length)]}*\n${byearray}`);
	},
};