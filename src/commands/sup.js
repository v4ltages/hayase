// Utils contains the array.
const { suparray } = require('../../utils');
const { SlashCommandBuilder } = require('@discordjs/builders');

const responceList = [
	'says su~p! 👋',
	'says heyo! 👋',
	'says hi! 👋',
	'says hiya! 👋',
	'says hey! 👋',
];

module.exports = {
	data: new SlashCommandBuilder()
		.setName('sup')
		.setDescription('Say hi to anyone.')
		.addStringOption(option => option.setName('input').setDescription('What you want it to say or mention?')),
	async execute(interaction) {
		const input = interaction.options.getString('input');
		if (input) return interaction.reply(`${interaction.member} *says su~p* to ${input}\n${suparray[Math.floor(Math.random() * suparray.length)]}`);
		return interaction.reply(`${interaction.member} *${responceList[Math.floor(Math.random() * responceList.length)]}*\n${suparray[Math.floor(Math.random() * suparray.length)]}`);
	},
};
