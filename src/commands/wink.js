const { SlashCommandBuilder } = require('@discordjs/builders');
const { winkarray } = require('../../utils');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('wink')
		.setDescription(';)')
		.addUserOption(option => option.setName('mention').setDescription('Select a user to wink at.')),
	async execute(interaction) {
		const user = interaction.options.getUser('mention');
		if (user) return interaction.reply(`${interaction.member} winked at ${user}\n${winkarray[Math.floor(Math.random() * winkarray.length)]}`);
		return interaction.reply(`${interaction.member} gives a wink...\n${winkarray[Math.floor(Math.random() * winkarray.length)]}`);
	},
};
