const { SlashCommandBuilder } = require('@discordjs/builders');
const { smilearray } = require('../../utils');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('smile')
		.setDescription('Smile at someone ur proud of.')
		.addUserOption(option => option.setName('mention').setDescription('Select a user to proud of.')),
	async execute(interaction) {
		const user = interaction.options.getUser('mention');
		if (user) return interaction.reply(`${interaction.member} gives a nice smile to ${user}\n ${smilearray[Math.floor(Math.random() * smilearray.length)]}`);
		interaction.reply(`${interaction.member} smiles. \n${smilearray[Math.floor(Math.random() * smilearray.length)]}`);
	},
};