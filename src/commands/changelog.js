const axios = require('axios');
const { MessageEmbed } = require('discord.js');
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('changelogs')
		.setDescription('List of things that have been changed with Hayase.'),
	async execute(interaction) {
		axios.get('https://gitlab.com/api/v4/projects/18199726/repository/files/CHANGELOG/raw?ref=master').then(response => {
			const changelog = new MessageEmbed()
				.setColor('#df4329')
				.setTitle('Changelog for Hayase')
				.setDescription(response.data)
				.setTimestamp();
			return interaction.reply({ embeds: [changelog], ephemeral: true });
		})
			.catch((error) => {
				if (error.response) {
					interaction.reply({ text: 'Sorry, the request was made and the server responded with a status code that falls out of the range of 2xx', ephemeral: true });
				}
				else if (error.request) {
					console.log(error.request);
					interaction.reply({ text: 'Sorry, no response was received...', ephemeral: true });
				}
			});
	},
};