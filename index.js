// Require Discord.js to be downloaded.
const { Client, Intents, Collection } = require('discord.js');
// Dotenv for auth
require('dotenv/config');

// Filesystem manipulation
const fs = require('fs');
const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] });

// Creates a command collection
client.commands = new Collection();
const commandFiles = fs.readdirSync('./src/commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
	const command = require(`./src/commands/${file}`);
	client.commands.set(command.data.name, command);
}

client.on('ready', () => {
	console.clear();
	console.log('[INIT] Bot is now online');
	console.log('[INFO] Currently in ' + client.guilds.cache.size + ' guilds');
	const statuses = [
		'senpai drawing...',
		'senpai being a pervert...',
		client.guilds.cache.size + ' servers...',
		'do / to see my commands...',
		'over senpai...',
		'https://hayase.voltages.me/',
	];
	setInterval(function() {
		const status = statuses[Math.floor(Math.random() * statuses.length)];
		client.user.setActivity(status, { type: 'WATCHING' });
	}, 20000);
});

// TopGG stats updater
// Optional dependency, can be removed
const { AutoPoster } = require('topgg-autoposter');

const ap = AutoPoster(process.env.TOPGG_TOKEN, client);

ap.on('posted', () => {
	console.log('[STAT] Posted client stats to Top.gg');
});

// Switching to using slash commands notice to users
// Will be removed at a later date
client.on('messageCreate', message => {
	if (!message.content.startsWith('h!') || message.author.bot) return;

	try {
		return message.reply('Due to Discord changing the way sending messages works, message functionality has been replaced with slash commands. Use / to see what commands are available.\nSee https://dis.gd/mcfaq').then(delmsgresponce => {
			setTimeout(() => delmsgresponce.delete(), 12000);
		});
	}
	catch (error) {
		console.error(error);
		message.channel.send('I ran into a issue while running this, please use /issue to report how this occurred.');
	}

});

client.on('interactionCreate', async interaction => {
	if (!interaction.isCommand()) return;

	const command = client.commands.get(interaction.commandName);

	if (!command) return;

	try {
		await command.execute(interaction);
	}
	catch (error) {
		console.error(error);
		await interaction.reply({ content: 'I ran into a issue while running this, please use /issue to report how this occurred.', ephemeral: true });
	}
});

client.login(process.env.DISCORD_TOKEN);