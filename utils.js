const smugarray = [
	'https://volt.hcink.org/static/nagatoro/84.png',
	'https://volt.hcink.org/static/nagatoro/85.png',
	'https://volt.hcink.org/static/nagatoro/95.png',
	'https://volt.hcink.org/static/nagatoro/101.png',
	'https://volt.hcink.org/static/nagatoro/230.png',
	'https://volt.hcink.org/static/nagatoro/260.png',
	'https://volt.hcink.org/static/nagatoro/264.png',
];

const happyarray = [
	'https://volt.hcink.org/static/nagatoro/16.jpg',
	'https://volt.hcink.org/static/nagatoro/42.jpg',
	'https://volt.hcink.org/static/nagatoro/67.jpg',
	'https://volt.hcink.org/static/nagatoro/91.png',
	'https://volt.hcink.org/static/nagatoro/131.png',
	'https://volt.hcink.org/static/nagatoro/146.png',
	'https://volt.hcink.org/static/nagatoro/183.png',
	'https://volt.hcink.org/static/nagatoro/241.png',
	'https://volt.hcink.org/static/nagatoro/261.png',
	'https://volt.hcink.org/static/nagatoro/306.png',
];

const suparray = [
	'https://volt.hcink.org/static/nagatoro/7.jpg',
	'https://volt.hcink.org/static/nagatoro/33.jpg',
	'https://volt.hcink.org/static/nagatoro/111.png',
	'https://volt.hcink.org/static/nagatoro/294.png',
	'https://volt.hcink.org/static/nagatoro/1045.png',
	'https://volt.hcink.org/static/nagatoro/1168.png',
	'https://volt.hcink.org/static/nagatoro/1276.png',
];

const smirkarray = [
	'https://volt.hcink.org/static/nagatoro/58.jpg',
	'https://volt.hcink.org/static/nagatoro/173.png',
	'https://volt.hcink.org/static/nagatoro/142.png',
	'https://volt.hcink.org/static/nagatoro/182.png',
	'https://volt.hcink.org/static/nagatoro/197.png',
	'https://volt.hcink.org/static/nagatoro/213.png',
];

const smilearray = [
	'https://volt.hcink.org/static/nagatoro/93.png',
	'https://volt.hcink.org/static/nagatoro/109.png',
	'https://volt.hcink.org/static/nagatoro/123.png',
	'https://volt.hcink.org/static/nagatoro/139.png',
	'https://volt.hcink.org/static/nagatoro/174.png',
	'https://volt.hcink.org/static/nagatoro/225.png',
	'https://volt.hcink.org/static/nagatoro/231.png',
];

const gigglearray = [
	'https://volt.hcink.org/static/nagatoro/50.jpg',
	'https://volt.hcink.org/static/nagatoro/92.png',
	'https://volt.hcink.org/static/nagatoro/103.png',
	'https://volt.hcink.org/static/nagatoro/150.png',
	'https://volt.hcink.org/static/nagatoro/179.png',
	'https://volt.hcink.org/static/nagatoro/224.png',
	'https://volt.hcink.org/static/nagatoro/240.png',
];

const winkarray = [
	'https://volt.hcink.org/static/nagatoro/482.png',
	'https://volt.hcink.org/static/nagatoro/651.png',
	'https://volt.hcink.org/static/nagatoro/703.png',
	'https://volt.hcink.org/static/nagatoro/744.png',
	'https://volt.hcink.org/static/nagatoro/796.png',
	'https://volt.hcink.org/static/nagatoro/890.png',
	'https://volt.hcink.org/static/nagatoro/909.png',
	'https://volt.hcink.org/static/nagatoro/910.png',
];

const grossarray = [
	'https://volt.hcink.org/static/nagatoro/70.jpg',
	'https://volt.hcink.org/static/nagatoro/503.png',
	'https://volt.hcink.org/static/nagatoro/763.png',
	'https://volt.hcink.org/static/nagatoro/870.png',
	'https://volt.hcink.org/static/nagatoro/902.png',
	'https://volt.hcink.org/static/nagatoro/1210.png',
];

const virginarray = [
	'https://hayase.voltages.me/d/youreavirginhuh.gif',
];

const byearray = [
	'https://hayase.voltages.me/d/bye.gif',
];

const duelarray = [
	'https://hayase.voltages.me/d/duel/duel1.png',
	'https://hayase.voltages.me/d/duel/duel2.png',
	'https://hayase.voltages.me/d/duel/duel3.png',
];


exports.smugarray = smugarray;
exports.happyarray = happyarray;
exports.suparray = suparray;
exports.smirkarray = smirkarray;
exports.smilearray = smilearray;
exports.gigglearray = gigglearray;
exports.winkarray = winkarray;
exports.grossarray = grossarray;
exports.virginarray = virginarray;
exports.byearray = byearray;
exports.duelarray = duelarray;
